__author__ = "xpresso.ai"
import argparse
import os

# try:
#     import pyspark
# except:
#     import findspark
#     findspark.init()
#     import pyspark

from app.indexer import CustomStringIndexer
from pyspark.sql import SparkSession
from pyspark.ml import Pipeline

if __name__ == "__main__":
    print('Running...', flush=True)

    parser = argparse.ArgumentParser()
    parser.add_argument('--arg-name-1', type=str, default='arg-value-1',
                        help='args 1 help')
    parser.add_argument('--arg-name-2', type=str, default='arg-value-2',
                        help='arg 2 help')
    parser.add_argument('--run_name', type=str, default='run_name-value',
                        help='run_name help')
    args = parser.parse_args()
    run_id = None
    if args:
        run_id = args.run_name
    print(f'All: {args}', flush=True)

    spark = SparkSession \
        .builder \
        .appName(run_id) \
        .getOrCreate()

    HDFS_HOST = 'hdfs://172.16.1.81:8020'
    HDFS_BASE_PATH = f"{HDFS_HOST}/user/xprops/dataset/h1b/h-1b-visa"
    HDFS_INPUT_TRAIN_DATA_LOCATION = f'{HDFS_BASE_PATH}/input'
    HDFS_OUTPUT_DATA_LOCATION = f'{HDFS_BASE_PATH}/output-{run_id}'

    data_df = spark.read.load(HDFS_INPUT_TRAIN_DATA_LOCATION, \
                            format="csv", \
                            inferSchema="true", \
                            header="true", \
                            mode="DROPMALFORMED")

    categoricalCol = 'EMPLOYER_NAME'

    indexer = CustomStringIndexer(categoricalCol+'-indexer', \
                                    run_id, \
                                    inputCol = categoricalCol, \
                                    outputCol = categoricalCol+'Index')
    t = indexer.fit(data_df)
    data_df = t.transform(data_df)
    data_df.write.parquet(HDFS_OUTPUT_DATA_LOCATION)
    
    indexer.completed()
    print(run_id, flush=True)
    spark.stop()