__all__ = ["ExperimentFieldName"]
__author__ = ["Srijan Sharma"]

from enum import Enum


class ExperimentFieldName(Enum):
    """
    Enum class to standardize all the database collection field names
    """

    EXPERIMENT_ID = "experiment_id"
    PIPELINE_EXPERIMENT_ID = "pipeline_experiment_id"
    PROJECT_NAME = "project_name"
    PIPELINE_NAME = "pipeline_name"
    PIPELINE_VERSION = "pipeline_version"
    EXPERIMENT_NAME = "experiment_name"
    COMPONENT_NAME = "component_name"
    TIMESTAMP = "timestamp"
    MASTER_NODE = "master_node"
    TARGET_CLUSTER = "target_cluster"
    TARGET_ENV = "target_environment"
    DESCRIPTION = "description"
    PIPELINE_FLAVOR = "pipeline_flavor"
    PIPELINE_ID = "pipeline_id"

    RUN_ID = "run_id"
    STATUS = "status"
    METRIC = "metric"
    RESULTS = "result"
    PIPELINE_RUN_ID = "pipeline_run_id"
    RUN_STATUS = "run_status"
    RUN_NAME = "run_name"
    RUN_DESCRIPTION = "description"
    RUN_DATA = "data"
    RUN_INPUTS = "run_parameters"
    RUN_STARTED_BY = "started_by"
    RUN_START_TIME = "start time"
    RUN_END_TIME = "end_time"
    RUN_OUTPUT = "output"
    RUN_RESULTS = "results"
    RUN_COMMIT = "commit"
    XPRESSO_RUN_NAME = "xpresso_run_name"

    PIPELINE_DASHBOARD_URL = "pipeline_dashboard_url"

    # Kubeflow Manager Fields

    def __str__(self):
        return self.value

    @staticmethod
    def generate_xpresso_run_name_alphanum(xpresso_run_name: str):
        """ Removes all special character and keeps alphanumeric and underscore"""
        return xpresso_run_name.replace("-", "_").replace(".", "_")