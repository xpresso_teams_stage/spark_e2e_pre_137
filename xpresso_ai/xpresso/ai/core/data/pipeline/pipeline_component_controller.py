__all__ = ["PipelineController"]
__author__ = "KK"

import os
import pickle
import shutil
from datetime import datetime

from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    PipelineRunException, ControllerClientResponseException, \
    CreateBranchException
from xpresso.ai.core.commons.network.http.http_request import HTTPMethod
from xpresso.ai.core.commons.network.http.send_request import SendHTTPRequest
from xpresso.ai.core.commons.utils.constants import CONTROLLER_FIELD, \
    SERVER_URL_FIELD, STATUS, METRIC, OUTPUT_DIR, DESCRIPTION, \
    TIMEFORMAT, PKL_EXT, BRANCH_EXTENTION, MESSAGE, PROJECT_NAME_IN_PROJECTS, \
    DV_PROJECT_TOKEN, PROJECT_TOKEN_IN_PROJECTS
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.data.pipeline.ExperimentFieldName \
    import ExperimentFieldName
from xpresso.ai.core.data.pipeline.FieldValue import FieldValue
from xpresso.ai.core.data.pipeline.component_messages import ComponentMessage
from xpresso.ai.core.data.versioning.controller_factory import \
    VersionControllerFactory
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.utils.api_utils import APIUtils


class PipelineController:
    OUTPUT_DIR = OUTPUT_DIR

    def __init__(self):
        self.config = XprConfigParser()
        server_url = self.config[CONTROLLER_FIELD][SERVER_URL_FIELD]
        self.server_endpoint_run = f"{server_url}/run/api"
        self.server_endpoint_experiment = f"{server_url}/exp/api"
        self.request_module = SendHTTPRequest()
        self.logger = XprLogger()
        self.api_utils = APIUtils()

    def pipeline_component_started(self, xpresso_run_name, component_name):

        self.report_pipeline_status(xpresso_run_name, component_name,
                                    {STATUS: {
                                        MESSAGE: ComponentMessage.COMPONENT_STARTED_MESSAGE.value}})

    def pipeline_component_terminated(self, xpresso_run_name, component_name):

        # delete any state file
        self.clean_state(xpresso_run_name, component_name)
        self.report_pipeline_status(xpresso_run_name, component_name,
                                    {STATUS: {
                                        MESSAGE: ComponentMessage.COMPONENT_TERMINATED_MESSAGE.value}})

    def pipeline_component_paused(self, xpresso_run_name, component_name,
                                  component_state, push_exp=True):

        # delete any state file
        self.save_state(xpresso_run_name, component_name, component_state)
        if push_exp:
            self.push_run_version(xpresso_run_name)
        self.report_pipeline_status(xpresso_run_name, component_name,
                                    {STATUS: {
                                        MESSAGE: ComponentMessage.COMPONENT_PAUSED_MESSAGE.value}})

        self.clean_state(xpresso_run_name, component_name)

    def pipeline_component_restarted(self, xpresso_run_name, component_name):

        # delete any state file
        self.clean_state(xpresso_run_name, component_name)
        self.pull_run_version(xpresso_run_name)
        component_state = self.load_state(xpresso_run_name, component_name)
        self.report_pipeline_status(xpresso_run_name, component_name,
                                    {STATUS: {
                                        MESSAGE: ComponentMessage.COMPONENT_RESTARTED_MESSAGE.value}})
        return component_state

    def pipeline_component_completed(self, xpresso_run_name, component_name,
                                     results,
                                     push_exp=False):

        # delete any state file
        self.report_pipeline_status(xpresso_run_name, component_name,
                                    {STATUS: {
                                        MESSAGE: ComponentMessage.COMPONENT_COMPLETED_MESSAGE.value}})

        if push_exp:
            self.push_run_version(xpresso_run_name)
        self.update_field(xpresso_run_name,
                          field_name=ExperimentFieldName.RESULTS.value,
                          field_value=results)
        self.update_field(xpresso_run_name,
                          field_name=ExperimentFieldName.RUN_STATUS.value,
                          field_value=FieldValue.RUN_STATUS_COMPLETED.value)
        self.clean_state(xpresso_run_name, component_name)

    def update_field(self, xpresso_run_name,
                     field_name=ExperimentFieldName.RUN_STATUS.value,
                     field_value=FieldValue.RUN_STATUS_RUNNING.value):
        """
            Updates the value of the provided field name with the field_value
            Args:
                xpresso_run_name (str) : Unique identifier of that run instance
                field_name(str): Name of the key to be updated
                field_value : updated value
            Return: True if successful, else raise suitable Exception
        """

        error_message = "Unable to update field : {}, with value {}, for run " \
                        "named : {}".format(field_name, field_value,
                                            xpresso_run_name)

        update_body = {
            ExperimentFieldName.XPRESSO_RUN_NAME.value: xpresso_run_name,
            field_name: field_value
        }
        try:
            self.request_module.send(
                self.server_endpoint_run, HTTPMethod.PUT, update_body
            )
            self.logger.info("Successfully updated the field : {} with value " \
                             "{} for run with name : {}".format(field_name,
                                                                field_value,
                                                                xpresso_run_name))

        except ControllerClientResponseException as e:
            self.logger.error(error_message)
            raise PipelineRunException(e.message)

        except Exception:
            self.logger.error(error_message)
            raise PipelineRunException()

    def get_pipeline_run_status(self, xpresso_run_name):

        """
            fetches the run status corresponding to the run name
            Args:
                xpresso_run_name (str) : Unique identifier of that run instance
            Return: Run status(RUNNING,COMPLETED etc)
        """

        error_message = "Unable to get pipeline run status for run named : " \
                        "{}".format(xpresso_run_name)

        search_filter = {
            ExperimentFieldName.XPRESSO_RUN_NAME.value: xpresso_run_name}
        try:
            response = self.request_module.send(
                self.server_endpoint_run, HTTPMethod.GET, search_filter
            )
            record = response["results"]
            if not len(record):
                msg = "Unable to get pipeline run status, No entry present in the " \
                      "database for provided ID :" \
                      "{}".format(xpresso_run_name)
                self.logger.error(msg)
                raise PipelineRunException(message=msg)

            run_status = record[ExperimentFieldName.RUN_STATUS.value]
            self.logger.info("Retrieved Run status for Run Id {} is"
                             "{}".format(xpresso_run_name, run_status))
            return str(run_status)

        except ControllerClientResponseException as e:
            self.logger.error(error_message)
            raise PipelineRunException(e.message)

        except Exception:
            self.logger.error(error_message)
            raise PipelineRunException()

    def report_pipeline_status(self, xpresso_run_name, component_name, status):
        """
            Updates the status for the experiment  corresponding to the
            xpresso_run_names
            Args:
                xpresso_run_name (str) : Unique identifier of that run instance
                component_name(str): Name of the component
                status : latest status of the component
            Return: True if successful, else raise suitable Exception
        """

        error_message = "Unable to updated pipeline status for component : " \
                        "{}, to : {} , for run named :{}".format(
            component_name, status, xpresso_run_name)

        now = datetime.now()
        curr_time = now.strftime(TIMEFORMAT)

        try:
            search_filter = {
                ExperimentFieldName.XPRESSO_RUN_NAME.value: xpresso_run_name}

            response = self.request_module.send(
                self.server_endpoint_run, HTTPMethod.GET, search_filter
            )

            if "results" not in response or not len(response["results"]):
                msg = "Unable to report pipeline status, No entry present in the " \
                      "database for provided ID :" \
                      "{}".format(xpresso_run_name)
                self.logger.error(msg)
                raise PipelineRunException(message=msg)

            record = response["results"]
            update_body = {
                ExperimentFieldName.XPRESSO_RUN_NAME.value: xpresso_run_name,
            }

            saved_status = record[ExperimentFieldName.STATUS.value]
            if not len(saved_status):
                self.logger.info("Already present status value is empty."
                                 "Updating Status.")
                saved_status = list()

            current_status = {ExperimentFieldName.TIMESTAMP.value: curr_time,
                              ExperimentFieldName.COMPONENT_NAME.value: component_name}

            if STATUS in status.keys():
                current_status[STATUS] = dict()
                for key, val in status[STATUS].items():
                    current_status[STATUS][key] = val

            if METRIC in status.keys():
                current_status[METRIC] = dict()
                for key, val in status[METRIC].items():
                    current_status[key] = val

            saved_status.append(current_status)
            update_body[ExperimentFieldName.STATUS.value] = saved_status

            self.request_module.send(
                self.server_endpoint_run, HTTPMethod.PUT, update_body
            )
            self.logger.info("Updated updated the Status for Run : {} using "
                             "{}".format(str(xpresso_run_name), status))

        except ControllerClientResponseException as e:
            self.logger.error(error_message)
            raise PipelineRunException(e.message)

        except Exception as e:
            print(str(e))
            print(error_message)
            self.logger.exception(error_message)
            raise PipelineRunException()

    def push_run_version(self, xpresso_run_name):

        """
            Pushes the latest version to pachyderm
            Args:
                xpresso_run_name (str) : Unique identifier of that run instance
            Return: True if successful, else raise suitable Exception
        """
        error_message = "Unable to get version model for run named : " \
                        "{}".format(xpresso_run_name)

        try:
            response = self.request_module.send(
                self.server_endpoint_run, HTTPMethod.GET,
                {ExperimentFieldName.XPRESSO_RUN_NAME.value: xpresso_run_name}
            )
            record = response["results"]

            if not len(record):
                msg = "Unable to get pipeline run status, No entry present in the " \
                      "database for provided ID :" \
                      "{}".format(xpresso_run_name)
                self.logger.error(msg)
                raise PipelineRunException(message=msg)

            saved_commit = record[ExperimentFieldName.RUN_OUTPUT.value]
            expt_id = record[ExperimentFieldName.EXPERIMENT_ID.value]

            response_expt = self.request_module.send(
                self.server_endpoint_experiment, HTTPMethod.GET,
                {ExperimentFieldName.EXPERIMENT_ID.value: expt_id}
            )

            project_name = \
                response_expt[ExperimentFieldName.PROJECT_NAME.value]

            branch_name = ExperimentFieldName.generate_xpresso_run_name_alphanum(xpresso_run_name + BRANCH_EXTENTION)
            kwargs = {
                "repo_name": project_name,
                "branch_name": branch_name,
                "path": os.path.dirname(OUTPUT_DIR),
                "data_type": "files",
                "dataset_name": ExperimentFieldName.generate_xpresso_run_name_alphanum(xpresso_run_name),
                "description": DESCRIPTION
            }

            version_controller = self.fetch_version_controller(project_name)
            try:
                version_controller.create_branch(
                    **{"repo_name": project_name, "branch_name": branch_name})
            except CreateBranchException:
                self.logger.warning("Branch Already present")

            commit_id, path = version_controller.push_dataset(**kwargs)

            saved_commit.append({
                "commit_id": commit_id,
                "path": path
            })

            self.request_module.send(
                self.server_endpoint_run, HTTPMethod.PUT,
                {
                    ExperimentFieldName.XPRESSO_RUN_NAME.value: xpresso_run_name,
                    ExperimentFieldName.RUN_OUTPUT.value: saved_commit
                }
            )
            self.logger.info("Successfully versioned model for run : "
                             "{} with commit id : {}".format(xpresso_run_name,
                                                             commit_id))

        except ControllerClientResponseException as e:
            self.logger.exception(error_message)
            raise PipelineRunException(e.message)

        except Exception as e:
            self.logger.exception(str(e))
            raise PipelineRunException(str(e))

        return

    def pull_run_version(self, xpresso_run_name):
        """
            Pulls the latest version from pachyderm
            Args:
                xpresso_run_name (str) : Unique identifier of that run instance
            Return: True if successful, else raise suitable Exception
        """
        error_message = "Unable to get version model for run named : " \
                        "{}".format(xpresso_run_name)

        try:
            response = self.request_module.send(
                self.server_endpoint_run, HTTPMethod.GET,
                {ExperimentFieldName.XPRESSO_RUN_NAME.value: xpresso_run_name}
            )
            record = response["results"]

            if not len(record):
                msg = "Unable to get pipeline run status, No entry present in the " \
                      "database for provided ID :" \
                      "{}".format(xpresso_run_name)
                self.logger.error(msg)
                raise PipelineRunException(message=msg)

            if not len(record[ExperimentFieldName.RUN_OUTPUT.value]):
                msg = "No model commits present in the dataset for {}. " \
                      "Unable to fetch the last version".format(
                        xpresso_run_name)
                self.logger.error(msg)
                raise PipelineRunException(message=msg)

            expt_id = record[ExperimentFieldName.EXPERIMENT_ID.value]

            response_expt = self.request_module.send(
                self.server_endpoint_experiment, HTTPMethod.GET,
                {ExperimentFieldName.EXPERIMENT_ID.value: expt_id}
            )

            project_name = response_expt["results"][
                ExperimentFieldName.PROJECT_NAME.value]

            kwargs = {
                "repo_name": project_name,
                "branch_name": xpresso_run_name + BRANCH_EXTENTION,
                "path": os.path.dirname(OUTPUT_DIR),
                "output_type": "files",
            }

            version_controller = self.fetch_version_controller(project_name)
            output_path = version_controller.pull_dataset(**kwargs)
            output_path = os.path.abspath(
                os.path.join(os.getcwd(), output_path))

            for file in os.listdir(output_path):
                shutil.move(os.path.join(output_path, file), OUTPUT_DIR)

            shutil.rmtree(output_path)

        except ControllerClientResponseException as e:
            self.logger.error(error_message)
            raise PipelineRunException(e.message)

        except Exception:
            self.logger.error(error_message)
            raise PipelineRunException()

        return

    def save_state(self, xpresso_run_name, component_name, state):
        print("Saving state", flush=True)
        print(state, flush=True)
        pickle_out = open(
            self.get_state_filename(xpresso_run_name, component_name),
            "wb")
        pickle.dump(state, pickle_out)
        pickle_out.close()
        return

    def load_state(self, xpresso_run_name, component_name):
        print("Loading state", flush=True)
        state_file = self.get_state_filename(xpresso_run_name, component_name)
        state = None
        if os.path.exists(state_file):
            pickle_in = open(state_file, "rb")
            state = pickle.load(pickle_in)
            pickle_in.close()
        print(state, flush=True)
        return state

    def get_state_filename(self, xpresso_run_name, component_name):
        return os.path.join(OUTPUT_DIR, str(xpresso_run_name) + "_" +
                            component_name + PKL_EXT)

    def clean_state(self, xpresso_run_name, component_name):
        state_file = self.get_state_filename(xpresso_run_name=xpresso_run_name,
                                             component_name=component_name)
        if os.path.exists(state_file):
            os.remove(state_file)

    def fetch_version_controller(self, project_name: str):
        """
        generates new instance of version controller for data versioning
         using project_token fetched using project_name

        Args:
            project_name: name of the project
        Returns:
             returns version controller object
        """
        response_project = self.api_utils.get_project_info(
            {PROJECT_NAME_IN_PROJECTS: project_name}
        )
        project_token = response_project[PROJECT_TOKEN_IN_PROJECTS]
        controller_factory = VersionControllerFactory(**{
            DV_PROJECT_TOKEN: project_token
        })
        version_controller = controller_factory.get_version_controller()
        return version_controller

