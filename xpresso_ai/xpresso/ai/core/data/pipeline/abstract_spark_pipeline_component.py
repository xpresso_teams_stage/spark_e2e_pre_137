__all__ = ["AbstractSparkPipelineEstimator", \
            "AbstractSparkPipelineTransformer", \
            "XprPipeline"]

__author__ = "Gagan"

import time
import argparse

from pyspark.sql import SparkSession
from pyspark.ml.pipeline import Pipeline
from pyspark.ml.pipeline import PipelineModel
from pyspark.ml.base import Estimator, Transformer

from xpresso.ai.core.commons.exceptions.xpr_exceptions import XprExceptions
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent


class AbstractSparkPipelineComponent(AbstractPipelineComponent):

    def __init__(self, name=None):
        super().__init__(name=name)
        self.should_thread_continue = False

    def completed(self, push_exp=False):

        self.logger.info("Parent component completed")

        self.controller.pipeline_component_completed(self.xpresso_run_name,
                                                     self.name, self.results,
                                                     push_exp=push_exp)
        self.should_thread_continue = False


class AbstractSparkPipelineEstimator(AbstractSparkPipelineComponent):

    def __init__(self, name=None):
        super().__init__(name=name)


class AbstractSparkPipelineTransformer(AbstractSparkPipelineComponent):

    def __init__(self, name=None):
        super().__init__(name=name)


class XprPipeline(Pipeline, AbstractSparkPipelineComponent):

    def __init__(self, sys_args):
        parser = argparse.ArgumentParser()
        parser.add_argument('--xpresso_run_name', type=str, default='xpresso_run_name-value',
                            help='xpresso_run_name help')
        args, unknown = parser.parse_known_args(sys_args)
        xpresso_run_name = args.xpresso_run_name
        self.spark_session = SparkSession \
            .builder \
            .appName(xpresso_run_name) \
            .getOrCreate()
        Pipeline.__init__(self)
        AbstractSparkPipelineComponent.__init__(self, name=xpresso_run_name)
        self.name = args.xpresso_run_name
        self.xpresso_run_name = args.xpresso_run_name
        self.should_thread_continue = False

    def _fit(self, dataset):
        stages = self.getStages()
        for stage in stages:
            if not (isinstance(stage, Estimator) or isinstance(stage, Transformer)):
                raise TypeError(
                    "Cannot recognize a pipeline stage of type %s." % type(stage))

        indexOfLastEstimator = -1
        for i, stage in enumerate(stages):
            if isinstance(stage, Estimator):
                indexOfLastEstimator = i
        transformers = []
        for i, stage in enumerate(stages):
            start_time = time.time()
            if i <= indexOfLastEstimator:
                if isinstance(stage, Transformer):
                    transformers.append(stage)
                    dataset = stage.transform(dataset)
                else:  # must be an Estimator
                    model = stage.fit(dataset)
                    transformers.append(model)
                    if i < indexOfLastEstimator:
                        dataset = model.transform(dataset)
            else:
                transformers.append(stage)

            stage.state = dataset

            # report the status only when stage is 
            # xpresso's component as well.
            # to retain native pyspark compatibility
            if isinstance(stage, AbstractPipelineComponent):
                status = {'status' : 
                        {'status' : f'Component completed {stage.name}'}
                    }
                try:
                    stage.report_status(status)
                except Exception as e:
                    print('Spark transformers not init yet, ignore.')
                    print(str(e))
            stage.completed()
        
        print(f'Done now returning PipelineModel', flush=True)
        return PipelineModel(transformers)
