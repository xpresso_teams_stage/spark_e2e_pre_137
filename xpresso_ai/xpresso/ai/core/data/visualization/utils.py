# Developed By Nalin Ahuja, nalin.ahuja@abzooba.com

import os
import string
import random

# Output Types and Paths

HTML = "html"
DEFAULT_PLOT_PATH = "./plots/"

PNG = "png"
DEFAULT_IMAGE_PATH = "./images/"

PDF = "pdf"
DEFAULT_PDF_PATH = "./pdf/"

# Constant for maximum number of xticks for a bar chart
DEFAULT_XTICKS_NUMBER = 20

DECIMAL_PRECISION = 3

X_LABEL = "x_label"
Y_LABEL = "y_label"
TARGET_LABEL = "target_label"


def make_container(output_path):
    """
    Function creates a folder for rendered plots
    Args:
        output_path(:str): Path to be created
    """
    if not (os.path.isdir(str(output_path))):
        os.makedirs(str(output_path), exist_ok=True)


def clear_terminal():
    """Function clears the terminal screen"""
    os.system('cls' if os.name == 'nt' else 'clear')


def concatenate_lists(list_1, list_2):
    """Function joins to lists"""
    joined_lists = []

    for elem in list_1:
        joined_lists.append(elem)
    for elem in list_2:
        joined_lists.append(elem)

    return joined_lists


def generate_id():
    """Function generates a render ID to differentiate plots"""
    return ''.join(
        random.choice(string.ascii_letters + string.digits) for i in range(5))


def detect_notebook():
    """Function detects runtime environment"""
    try:
        shell_type = get_ipython().__class__.__name__
        # Jupyter Notebook
        if shell_type == 'ZMQInteractiveShell':
            return True
    except NameError:
        # Standard Interpreter
        return False


class StopExecution(Exception):
    """Function halts the program"""

    def _render_traceback_(self):
        pass


def halt():
    if (detect_notebook()):
        raise StopExecution
    else:
        exit()
