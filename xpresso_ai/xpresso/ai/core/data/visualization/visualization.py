from enum import Enum
from xpresso.ai.core.data.visualization.plotly_visualization import \
    PlotlyVisualization
from xpresso.ai.core.data.visualization.seaborn_visualization import \
    SeabornVisualization
from xpresso.ai.core.logging.xpr_log import XprLogger


class VisualizationBackend(Enum):
    """

    Enum class that lists visualization libraries supported by
    Xpresso Visualization library

    """

    SEABORN = "seaborn"
    PLOTLY = "plotly"


class Visualization:

    @staticmethod
    def get_visualizer(dataset, backend=VisualizationBackend.SEABORN.value):
        """
            This method returns Visualization object of a specific visualization
            library
            Args:
                dataset(:obj StructuredDataset): Structured dataset object on
                which visualization to be performed
                backend(:str): Visualization library to be used.
            Returns: Visualization object
        """
        if backend.lower() == VisualizationBackend.SEABORN.value:
            return SeabornVisualization(dataset)
        elif backend.lower() == VisualizationBackend.PLOTLY.value:
            return PlotlyVisualization(dataset)
        else:
            XprLogger().error(
                "Invalid visualization library. Using default library(seaborn)")
            return SeabornVisualization(dataset)
