"""Report generation for distribution of data attribute wise"""
import os
from enum import Enum
from fpdf import FPDF

from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.data.exploration.data_type import DataType
from xpresso.ai.core.data.visualization import utils
from xpresso.ai.core.commons.utils.constants import PERCENT_SIGN, \
    LINE_SPACE


class ReportParam(Enum):
    """
    Enum class to standardize all the report types
    """
    UNIVARIATE = "univariate"
    MULTIVARIATE = "multivariate"
    COMBINED = "combined"
    SINGLEPAGE = "single"
    DOUBLEPAGE = "double"

    def __str__(self):
        return self.value


class Report(FPDF):
    """
    Report class inherits FPDF python module and provides functionality to
    create report
    Args:
        dataset (:obj: `StructuredDataset`): Input data and its information
                    along with metrics of all attributes.
    Attributes:
        dataset (:obj: `StructuredDataset`): Input data and its information
                    along with metrics of all attributes.
        logo_path (str): path for logo to be used in title page and header.
    """

    def __init__(self, dataset):

        super().__init__(orientation='P', unit='mm', format='A4')
        self.dataset = dataset
        config_parser = XprConfigParser()
        config_directory = os.path.dirname(
            config_parser.get_default_config_path())
        self.logo_path = os.path.join(config_directory,
                                      config_parser["visualization"][
                                          "logo_path"])

    def cell(self, width, height=0, text='', border=0, newline=0, align='',
             fill=0, link=''):
        """
        Prints a cell (rectangular area) with or without text
        Args:
            width (:int): Cell width. If 0, the cell extends up to the right
            margin.
            height (:int): Cell height. Default value: 0.
            text (:str): String to print. Default value: empty string.
            border (:int): Indicates if borders must be drawn around the
            cell. Default value: 0.
                The value can be either a number: 0: no border, 1: frame
                or a string containing some or all of the following
                characters (in any order):
                    L: left, T: top, R: right, B: bottom
            newline (:int): Indicates where the current position should go
            after the call. Possible values are:
                0: to the right
                1: to the beginning of the next line
                2: below
                Putting 1 is equivalent to putting 0 and calling Ln() just
                after. Default value: 0.
            align (:str): Allows to center or align the text. Possible
            values
            are:
                L or empty string: left align (default value), C: center,
                R: right align
            fill (:bool): Indicates if the cell background must be painted (
            true) or transparent
                (false). Default value: false.
            link (:str): URL or identifier returned by AddLink().
        """
        super().cell(width, height, text, border, newline, align, fill,
                     link)

    def set_font(self, family, style='', size=0):
        """
        Sets the font size and style
        Args:
            family (:str):Family font. It can be either a name defined by
            AddFont() or one of
                the standard families (case insensitive)
                It is also possible to pass an empty string. In that case,
                the current family is kept.
            style (:str): Font style. Possible values are (case
            insensitive):
                empty string: regular, B: bold, I: italic, U: underline,
                or any combination. The default value is regular.
                 Bold and italic styles do not apply to Symbol and
                 ZapfDingbats.
            size (:str): Font size in points. The default value is the
            current size.
                If no size has been specified since the beginning of the
                document, the value taken is 12.
        """
        super().set_font(family, style, size)

    def header(self):
        """Overrides FPDF header function to define custom header"""
        if self.page_no() == 1:  # No header for title page
            return
        # Set font style for header Times Italic 10pt
        self.set_font('Times', 'I', 10)
        # Parameter specification: 80-width, 10,-height, text within the
        # cell, 0-No border, 0-cursor to the right, 'L'-left aligned text
        self.cell(80, 10, str(self.dataset.name), 0, 0, 'L')
        # Logo (160, 8) coordinates of left top corner of image with width=33mm
        self.image(self.logo_path, 160, 8, 33)
        self.ln(5)  # 5 Line space

    # Page footer
    def footer(self):
        """Overrides FPDF footer function to define custom footer"""
        if self.page_no() == 1:  # No footer for title page
            return
        # Position at 1.5 cm from bottom, 4 cm from right
        self.set_xy(-40, -15)
        # Set font Times italic 10pt
        self.set_font('Times', 'I', 10)
        # Parameter specification: 20-width, 10,-height, text within the
        # cell, 0-No border, 0-cursor to the right, 'R'-Right aligned text
        self.cell(20, 10, "Page {}".format(self.page_no()), 0, 0, 'R')
        # Position cursor at 1.5 cm from bottom
        self.set_y(-15)
        # Parameter specification: 40-width, 10,-height, text within the
        # cell, 0-No border, 0-cursor to the right, 'L'-left aligned text
        self.cell(40, 10, 'Powered by xpresso.ai ', 0, 0, 'L')

    def title_page(self):
        """Generates the title page for the report"""

        self.add_page()
        # Set font Times no style 12pt
        self.set_font('Times', '', 12)
        # Logo (85, 20) coordinates of left top corner of image with width=40mm
        self.image(self.logo_path, 85, 20, 40)
        self.ln(50)  # 50 Line space
        # Set font Times bold 20pt
        self.set_font('Times', 'B', 20)
        # Parameter specification: 0-width(full width), 20-height,
        # text within the cell, 0-No border, 1-cursor on newline, 'C'-center
        # aligned text
        self.cell(0, 20, 'Report on', 0, 1, 'C')
        # Set font Times no style 16pt
        self.set_font('Times', '', 16)
        info = self.get_dataset_info()
        for key in info.keys():
            # Parameter specification: 0-width(full width), 20-height,
            # text within the cell, 0-No border, 1-cursor on newline,
            # 'C'-center aligned text
            self.cell(0, 20, "{} {}".format(key, info[key]), 0, 1, 'C')

    def overview_page(self):
        """Generates a page containing overview of the dataset"""
        self.add_page()
        self.ln(LINE_SPACE)  # 10 Line space
        self.set_font('Times', 'B', 20)
        # Parameter specification: 0-width(full width), 20-height,
        # text within the cell, 0-No border, 1-cursor on newline, 'C'-center
        # aligned text
        self.cell(0, 20, "OVERVIEW", 0, 1, 'C')
        data = self.get_dataset_overview(self.dataset.info)
        self.set_xy(10, 60)
        self.set_font('Times', 'B', 16)
        # Parameter specification: 0-width(full width), 20-height,
        # text within the cell, 0-No border, 1-cursor on newline, 'L'-center
        # aligned text
        self.cell(0, 20, "Dataset Info", 0, 1, 'L')
        self.set_font('Times', '', 12)
        for key in data:
            self.set_x(10)
            # Parameter specification: 60-width, 10,-height, text within the
            # cell, 0-No border, 0-cursor to the right, 'L'-left aligned text
            self.cell(60, 10, str(key), 0, 0, 'L')
            # Parameter specification: 30-width, 10,-height, text within the
            # cell, 0-No border, 1-cursor on newline, 'L'-left aligned text
            self.cell(30, 10, str(data[key]), 0, 1, 'L')
        data = self.get_attribute_overview(self.dataset.info)
        self.set_xy(130, 60)
        self.set_font('Times', 'B', 16)
        # Parameter specification: 0-width(full width), 20-height,
        # text within the cell, 0-No border, 1-cursor on newline, 'L'-left
        # aligned text
        self.cell(0, 20, "Attribute Types", 0, 1, 'L')
        self.set_font('Times', '', 12)
        for key in data:
            self.set_x(130)
            # Parameter specification: 60-width, 10,-height, text within the
            # cell, 0-No border, 0-cursor to the right, 'L'-left aligned text
            self.cell(60, 10, str(key), 0, 0, 'L')
            # Parameter specification: 30-width, 10,-height, text within the
            # cell, 0-No border, 1-cursor on newline, 'L'-left aligned text
            self.cell(30, 10, str(data[key]), 0, 1, 'L')

    def table(self, data=dict()):
        """
        Helper function to draw table of description of attribute
        Args:
            data (:obj: `dict`): data of description of the attribute
        """
        if not data:
            return
        self.set_font('Times', '', 12)
        for key in data.keys():
            # Parameter specification: 90-width, 6,-height, text within the
            # cell, 1-Frame border, 0-cursor to the right, 'C'-center aligned
            # text
            self.cell(90, 6, str(key), 1, 0, 'C')
            # Parameter specification: 90-width, 6,-height, text within the
            # cell, 1-Frame border, 1-cursor on newline, 'C'-center aligned text
            self.cell(90, 6, str(data[key]), 1, 1, 'C')

    def add_scatter_plots(self, input_path=utils.DEFAULT_IMAGE_PATH,
                          file_names=None):
        """
        Helper function to add scatter plots
        Args:
            input_path (str): path to the plots to be fetched into the
                report
            file_names(:list): list of scatter plot files to be added
        """
        if not os.path.exists(input_path) or not file_names:
            return
        self.add_multiple_images(file_names, input_path, header=None)

    def add_target_plots(self, attribute_name,
                         input_path=utils.DEFAULT_IMAGE_PATH, file_names=None):
        """
        Helper function to add target variable related plots
        Args:
            attribute_name(str): Attribute of which scattered plots to be
                added
            input_path (str): path to the plots to be fetched into the
                report
            file_names(:list): list of target variable plot files to be added
        """
        if not os.path.exists(input_path) or not file_names:
            return
        header = "Target variable analysis for {}".format(attribute_name)
        self.add_multiple_images(file_names, input_path, header)

    @staticmethod
    def get_metrics(attr):
        """
        Helper function to get description of the attribute
        Args:
            attr (:obj: `attributeInfo`): attribute of which the description
            is to be fetched
        Return:
            metrics (:obj: `dict`): Dictionary with attribute and its data
            information
        """
        metrics = dict()
        metrics["Attribute name"] = attr.name
        metrics["Attribute type"] = attr.type

        if attr.type == DataType.NUMERIC.value:

            metrics["NA count"] = "{} ({}{})".format(
                attr.metrics["na_count"],
                attr.metrics[
                    "na_count_percentage"],
                PERCENT_SIGN)
            metrics["Min value"] = attr.metrics["min"]
            metrics["Max value"] = attr.metrics["max"]
            metrics["Mean"] = attr.metrics["mean"]
            metrics["Median"] = attr.metrics["median"]
            metrics["Standard Deviation"] = attr.metrics["std"]

        elif attr.type == DataType.NOMINAL.value or attr.type == \
                DataType.ORDINAL.value:

            metrics["NA count"] = "{} ({}{})".format(
                attr.metrics["na_count"],
                attr.metrics[
                    "na_count_percentage"],
                PERCENT_SIGN)

        elif attr.type == DataType.DATE.value:

            metrics["NA count"] = "{} ({}{})".format(
                attr.metrics["na_count"],
                attr.metrics[
                    "na_count_percentage"],
                PERCENT_SIGN)
            metrics["Min value"] = attr.metrics["min"]
            metrics["Max value"] = attr.metrics["max"]

        return metrics

    @staticmethod
    def get_dataset_overview(dataset_info):
        """
        Helper function to get description of the attribute
        Args:
            dataset_info (:obj: `dataset_info`): dataset information which
            contains
             all dataset related metrics
        Return:
            data (:obj: `dict`): Dictionary with data information
        """
        data = dict()
        data["Number of records"] = dataset_info.metrics["num_records"]
        data["Number of attributes"] = dataset_info.metrics[
            "num_attributes"]
        data["NA count"] = "{} ({}{})".format(
            dataset_info.metrics["na_count"],
            dataset_info.metrics[
                "na_count_percentage"],
            PERCENT_SIGN)
        data["Missing count"] = "{} ({}{})".format(
            dataset_info.metrics["missing_count"],
            dataset_info.metrics["missing_count_percentage"], PERCENT_SIGN)
        data["Duplicate count"] = "{} ({}{})".format(
            dataset_info.metrics["duplicate_count"],
            dataset_info.metrics["duplicate_count_percentage"],
            PERCENT_SIGN)

        return data

    @staticmethod
    def get_attribute_overview(dataset_info):
        """
        Helper function to get description of the attribute
        Args:
            dataset_info (:obj: `dataset_info`): dataset information which
            contains
             all dataset related metrics
        Return:
            data (:obj: `dict`): Dictionary with number of attributes of
            particular Datatype
        """
        data = dict()
        data["Numeric"] = dataset_info.metrics["num_numeric"]
        data["Nominal"] = dataset_info.metrics["num_nominal"]
        data["Ordinal"] = dataset_info.metrics["num_ordinal"]
        data["Date"] = dataset_info.metrics["num_date"]
        data["String"] = dataset_info.metrics["num_string"]
        data["Text"] = dataset_info.metrics["num_text"]
        return data

    def get_dataset_info(self):
        """
        Helper function to get dataset information
        Return:
            info (:obj: `dict`): Dictionary with dataset information
        """
        info = dict()
        info["Dataset name:"] = self.dataset.name
        info["Created on:"] = self.dataset.created_on
        info["Created by:"] = self.dataset.created_by
        info["Project:"] = self.dataset.project
        info["Version:"] = self.dataset.version
        info["Description:"] = self.dataset.description
        return info

    def search_file(self, file_name, path):
        """ Search specific filename in output path
        Args:
            file_name(:str): filename to be searched
            path(:str): list of files
        """
        files = os.listdir(path)
        for file in files:
            temp_file_name = file.split("_")
            temp_file_name = "_".join(temp_file_name[:-1])
            if file_name == temp_file_name:
                return file
        return None

    def add_multiple_images(self, file_names, path, header):
        """
        Helper function to add multiple plots
        Args:
            file_names(:list): list of files to be added
            path(:str): path from which plots are to be fetched
            header(:str): Optional heading for the section
        """
        is_first_image = True
        for file in file_names:
            if not is_first_image:
                # Logo (10, 150) coordinates of left top corner of image with
                # width=180mm
                self.image(os.path.join(path, file), 10, 150,
                           180)
                is_first_image = True
                continue
            self.add_page()
            if header:
                self.set_font('Times', 'B', 16)
                # Parameter specification: 0-width(full le), 6,-height,
                # text within the cell, 1-Frame border, 1-cursor on newline,
                # 'C'-center aligned text
                self.cell(0, 20, header, 0, 1, 'C')
                self.ln(LINE_SPACE)
                header = None
            # Logo (10, 30) coordinates of left top corner of image with
            # width=180mm
            self.image(os.path.join(path, file), 10, 30, 180)
            is_first_image = False

    def attribute_file_mapping(self, input_path=utils.DEFAULT_IMAGE_PATH):
        files = os.listdir(input_path)
        attribute_file_mapping = dict()
        for attr in self.dataset.info.attributeInfo:
            univariate = list()
            scatter = list()
            target = list()
            for file in files:
                temp_file_name = file.split("_")
                # Removing file extension and Unique ID to extract attribute
                # name from file name
                temp_file_name = "_".join(temp_file_name[:-2])
                if attr.name != temp_file_name:
                    continue
                if "target" in file:
                    target.append(file)
                elif "scatter" in file:
                    scatter.append(file)
                else:
                    univariate.append(file)
                attribute_file_mapping[attr.name] = {"univariate": univariate,
                                                     "target": target,
                                                     "scatter": scatter}
        return attribute_file_mapping
