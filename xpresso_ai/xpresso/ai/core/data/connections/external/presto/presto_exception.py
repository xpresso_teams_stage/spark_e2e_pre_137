"""Module for creating user-defined exceptions"""


class Error(Exception):
    """Base Class for all exceptions"""
    pass


class PrestoIPMissingInConfig(Error):
    """Raised when IP is missing in the config file"""
    pass


class PrestoImportException(Error):
    """Raised when the import function fails in PrestoConnector class"""
    pass


class PrestoPortMissingInConfig(Error):
    """Raised when Port is missing in the config file"""
    pass


class PrestoCatalogMissingInConfig(Error):
    """Raised when the catalog is missing in the config file"""
    pass


class PrestoSchemaMissingInConfig(Error):
    """Raised when schema is missing in the config file"""
    pass


class PrestoTableMissingInConfig(Error):
    """Raised when table is missing in the config file"""
    pass


class PrestoConfigFileIsMissing(Error):
    """Raised when the config file is missing"""
    pass


class PrestoConnectionException(Error):
    """Raised when connection to Presto fails"""
    pass


class PrestoColumnsMissingInConfig(Error):
    """Raised when the columns attribute is missing."""
    pass
