""" Abstract class for Data-Connectivity """

__author__ = 'Shlok Chaudhari'
__all__ = 'connector'

import abc


class AbstractDataConnector(metaclass=abc.ABCMeta):
    """

    Abstract Base Class for the DataConnector

    """

    @classmethod
    @abc.abstractmethod
    def getlogger(cls):
        """

        Abstract method to instantiate the Xpresso Logger Module

        :return: Logger object

        """
        pass

    @classmethod
    @abc.abstractmethod
    def connect(cls, config):
        """

        Abstract method to establish client-side connection with
        various datasources

        :return: Client object to interact with datasource

        """
        pass

    @classmethod
    @abc.abstractmethod
    def close(cls):
        """

        Abstract method to close connection to a datasource

        :return: None

        """
        pass
