__author__ = 'Xpresso'

import importlib
import time
import os
import sys
import time
import argparse

if os.path.exists('packs.zip'):
    sys.path.insert(0, 'packs.zip')

try:
    import pyspark
except:
    import findspark
    findspark.init()
    import pyspark

from pyspark.sql import SparkSession
from pyspark.ml import Pipeline
from pyspark.ml.classification import RandomForestClassifier
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.sql.types import IntegerType

from string_indexer.app.indexer import CustomStringIndexer
from string_indexer.app.indexer import LabelIndexer
from one_hot_encoder.app.encoder import CustomerOneHotEncoderEstimator
from vector_assembler.app.assembler import CustomVectorAssembler

from xpresso.ai.core.data.pipeline.abstract_spark_pipeline_component \
    import XprPipeline

HDFS_BASE_PATH = "hdfs://172.16.1.81:8020/user/xprops/dataset/h1b/h-1b-visa"
HDFS_INPUT_TRAIN_DATA_LOCATION = HDFS_BASE_PATH + "/input"
HDFS_OUTPUT_DATA_LOCATION = HDFS_BASE_PATH + "/output-"


class MyPipeline(XprPipeline):

    def __init__(self, sys_args=[]):
        XprPipeline.__init__(self, sys_args)

    @staticmethod
    def main(args):
        """
        Accepts the command line args and start the pipeline

        Arguments:
            args {sys.argv} -- all command line args
        
        Returns:
            [MyPipeline] -- object of current class `MyPipeline`
            [DataFrame] -- the data frame object of the input data read
        """
        sys_args = args[1:]
        pipeline = MyPipeline(sys_args=sys_args)
        pipeline_stages = pipeline.prepare_stages()
        pipeline.setStages(pipeline_stages)
        return pipeline, pipeline.start()

    def read_data(self, path, format, inferSchema, header, mode):
        data_df = self.spark_session.read.load(path, \
            format=format, \
            inferSchema=inferSchema, \
            header=header, \
            mode=mode)
        return data_df

    def cast_col_int(self, dataframe, col_name):
        df = dataframe.withColumn(col_name, dataframe[col_name].cast(IntegerType()))
        df.printSchema()
        return df

    def start(self):
        """ 
        Start 
        Returns:
            [type] -- [description]
        """

        path = HDFS_INPUT_TRAIN_DATA_LOCATION
        data_df = self.read_data(path, "csv", "true", "true", "DROPMALFORMED")
        data_df.printSchema()
        col_name = 'PREVAILING_WAGE'
        df = self.cast_col_int(data_df, col_name)
        pm = self.fit(df)
        dataframe = pm.transform(df)
        dataframe.printSchema()
        print(f'Returning df...', flush=True)
        return dataframe
    
    def prepare_stages(self):
        """This can be used prepare the stages of the pipeline
        
        Returns:
            list -- list of XprComponents; and will be stages in the [Xpr|My]Pipeline
        """

        categoricalColumns = ['EMPLOYER_NAME', 'JOB_TITLE', 'FULL_TIME_POSITION', 'WORKSITE', 'YEAR']
        numericColumns = ['PREVAILING_WAGE']
        pipeline_stages = []

        for categoricalCol in categoricalColumns:
            stringIndexer = CustomStringIndexer(categoricalCol+'-indexer', self.xpresso_run_name, inputCol = categoricalCol, outputCol = categoricalCol + 'Index')
            encoder = CustomerOneHotEncoderEstimator(categoricalCol+'-encoder', self.xpresso_run_name, inputCols=[stringIndexer.getOutputCol()], outputCols=[categoricalCol + "classVec"])
            pipeline_stages += [stringIndexer, encoder]

        label_stringIdx = LabelIndexer('labelindexer', self.xpresso_run_name, inputCol = 'CASE_STATUS', outputCol = 'label')
        pipeline_stages += [label_stringIdx]

        assemblerInputs = [c + "classVec" for c in categoricalColumns] + numericColumns
        assembler = CustomVectorAssembler('assembler', self.xpresso_run_name, inputCols=assemblerInputs, outputCol="features")
        # assembler.setParams(handleInvalid="skip")
        pipeline_stages += [assembler]
        # pipeline stages design complete
        return pipeline_stages

    def stop(self, model):
        print(f'Not saving model for {self.xpresso_run_name}.')
        rfModel.save(f'{HDFS_OUTPUT_DATA_LOCATION}{self.xpresso_run_name}')
        self.completed()
        self.spark_session.stop()

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--model_param_numTrees', type=int, default=20,
                        help='model_param_numTrees help')
    parser.add_argument('--model_param_maxDepth', type=int, default=20,
                        help='model_param_maxDepth help')
    args, unknown = parser.parse_known_args(sys.argv[1:])

    # start the pipeline
    pipeline, df = MyPipeline.main(sys.argv)

    # infomational print
    df.printSchema()
    print(f"\nTraining Dataset Size: {df.count()}")

    # build the model
    num_trees = args.model_param_numTrees
    max_depth = args.model_param_maxDepth
    rf = RandomForestClassifier(featuresCol='features', \
                                labelCol = 'label', \
                                numTrees=num_trees, \
                                maxDepth=max_depth)
    rfModel = rf.fit(df)

    # MANDATORY - stop the pipeline, also saves the model
    pipeline.stop(rfModel)